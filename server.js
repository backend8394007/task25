const express = require('express');
const mongoose = require('mongoose');
const path = require('path');
const Form = require('./models/form');

const app = express();

const db = 'mongodb+srv://Eugene:23092004@cluster0.mgxwzah.mongodb.net/task25?retryWrites=true&w=majority';

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');
app.use(express.urlencoded({ extended: true }));
app.use(express.json());

mongoose
    .connect(db)
    .then(()=> console.log('Connected to DB'))
    .catch((err) => console.log(err));

app.get('/form/:id', (req, res) =>{
   const title = 'Анкета';
   res.render('form', {title});
});

app.post('/form/:id', async (req, res) =>{
   const {name, age, email, phone} = req.body;
   try {
      const form = new Form({ name, age, email, phone });
      const savedForm = await form.save();
      res.status(201).json(savedForm);
   }
   catch (error) {
      console.error('Error saving form:', error);
      res.status(500).json({ error: 'Internal Server Error' });
   }
});

const port = 3000;
app.listen(port, (err) => {
    err ? console.log(err) : console.log(`The server is listening on port: ${port}`);
});